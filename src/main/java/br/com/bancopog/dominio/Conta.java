package br.com.bancopog.dominio;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Random;

public class Conta {

	private BigDecimal saldo = BigDecimal.ZERO;
	private BigDecimal limite = BigDecimal.ZERO;

	private Cliente titular;
	private int numero;
	private Calendar dataAbertura;

	public Conta(int numero, Calendar dataAbertura) {
		this.numero = numero;
		this.dataAbertura = dataAbertura;
	}
	
	public Conta (Cliente titular,int numero, Calendar dataAbertura){
		this.titular=titular;
		this.numero = numero;
		this.dataAbertura = dataAbertura;
	}
	
	private static int geradorDeNumeros=1000;
	
	public static int geraNumeros(){
		geradorDeNumeros += new Random(42).nextInt(1000);
		return geradorDeNumeros++;
	}
	

	/**
      Removido o metodo setSaldo porque ele eh usado internamente apenas	 
	 * @param saldo
	 */


	public int getNumero() {
		return numero;
	}

	public Calendar getDataAbertura() {
		return dataAbertura;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public BigDecimal getLimite() {
		return limite;
	}

	public void setLimite(BigDecimal limite) {
		this.limite = limite;
	}

	public Cliente getTitular() {
		return titular;
	}

	/**
	 * Método sacar foi criado para delegar a responsabilidade para a classe conta,
	 *  que já possui todas as informações necessárias e suficientes para a operação.
	 * @param valor
	 */
	public void sacar(BigDecimal valor) throws RuntimeException{
		BigDecimal novoSaldo = this.getSaldo().subtract(valor);

		if (novoSaldo.compareTo(this.getLimite().negate()) >= 0) {
			this.saldo=novoSaldo;
		} else {
			throw new RuntimeException("Saldo insuficiente");
		}
	}
	
    /**
     * Método depositar foi criado na classe conta, que já possui todas as informações necessárias 
     * para a operação.
     * @param valor
     */
	public void depositar(BigDecimal valor){
		BigDecimal novoSaldo = this.getSaldo().add(valor);
		this.saldo=novoSaldo;
	}

	@Override
	public String toString() {
		return "{Conta #" + this.numero + "}";
	}
}
