package br.com.bancopog.servico;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Random;

import br.com.bancopog.dominio.Cliente;
import br.com.bancopog.dominio.Conta;
import br.com.bancopog.persistencia.Dao;

public class Banco {

	/**
	 * Consulta se o CPF tem pendencias no Serasa
	 * @param cpf
	 * @return true se estiver tudo ok; false se algo estiver errado
	 */
	public boolean consultaSerasa(String cpf) {
		// faz uma consulta ao web service do serasa
		return true;
	}
	
	/**
	 * Registra um novo cliente no sistema
	 * @param campoNome Recebe valor digitado no formulário com o nome.
	 * @param campoCpf  Recebe valor digitado no formulário com o cpf.
	 * @return Novo cliente persistido no sistema
	 */
	public Cliente registraCliente(String campoNome, String campoCpf)  throws SQLException {
		// recebe os dados de um formulário, cria um cliente e guarda no banco de dados
		Cliente cliente = new Cliente(campoNome,campoCpf);
		Dao<Cliente> dao = new Dao<Cliente>();
		dao.adiciona(cliente);
		
		return cliente;
	}
	
	
	/**
	 * Gera números para novas contas no banco
	 * @return Número da próxima conta
	 */
	public int geraNumeroConta() {
		return Conta.geraNumeros();
	}
	
	/**
	 * Registra nova conta no sistema
	 * @param titular Cliente titular da conta. Já deve ter sido aprovado na consulta ao Serasa.
	 * @param numero  Número da conta
	 * @return Nova Conta, aberta e registrada
	 */
	public Conta registraConta(Cliente titular, int numero) throws RuntimeException,SQLException {
		Calendar hoje = Calendar.getInstance();
		
		Conta novaConta = new Conta(titular,numero, hoje);
		
		
		Dao<Conta> dao = new Dao<Conta>();
		dao.adiciona(novaConta);
		
		return novaConta;
	}
}
