package br.com.bancopog.servico;

import java.math.BigDecimal;

import br.com.bancopog.dominio.Conta;

/**
 * Operações necessárias para nosso Banco
 */
public class OperacoesBancarias {

	/**
	 * Saca dinheiro de uma conta bancária
	 * 
	 * @param conta Conta onde sacar
	 * @param valor Valor a ser sacado
	 */
	public void saca(Conta conta, BigDecimal valor) {
		//alterado para usar o método delegado
		conta.sacar(valor);
	}
	
	/**
	 * Deposita dinheiro em uma conta bancária
	 * 
	 * @param conta Conta onde depositar
	 * @param valor Valor a ser sacado
	 */
	public void deposita(Conta conta, BigDecimal valor) {
		//alterado para usar o método delegado
		conta.depositar(valor);
	}
}
